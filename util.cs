using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Konsole;
using System.IO;
using System.Text.RegularExpressions;

namespace autoreddit_cmdline.util
{
    public class helpers
    {
        

        public static string deserialise(string JSON)
        {
            try
            {
                dynamic converted = JsonConvert.DeserializeObject<dynamic>(JSON);
                return converted[0].data.children[0].data.selftext;

            }
            catch (Exception err)
            {
                Console.WriteLine($"no one: \nthis program: {err.Message}");
                return "error";
            }
        }
        public static List<string> wordList = new List<string>();
        public static System.Speech.Synthesis.PromptBuilder builder = new System.Speech.Synthesis.PromptBuilder();
        public static void chunk(string chunkthis)
        {
            string[] s = chunkthis.Split('.');
            foreach (string _s in s)
            {
                if(!string.IsNullOrWhiteSpace(_s))
                {
                    builder.AppendBookmark(".");
                    builder.AppendText(_s);
                    wordList.Add(_s);
                }

            }
            renderer.renderer.render();
        }

        public static void log(string log)
        {
            File.AppendAllText(@"log.txt", $"------- START LOG ENTRY -------\ntime: {DateTime.UtcNow.Date}\n{log}\n------- END LOG ENTRY -------" + Environment.NewLine);
        }


    }



    public class Class2
    {
        public abstract class consoleCommand
        {
            public abstract string Name { get; protected set; }
            public abstract string Command { get; protected set; }
            public abstract string Description { get; protected set; }
            public abstract string Help { get; protected set; }
            public abstract void runCommand(string[] args);
            public void addCommandToConsole()
            {
                AddCommandsToConsole(Command, this);
            }

        }
        public static Dictionary<string, consoleCommand> Commands = new Dictionary<string, consoleCommand>();

        public static void AddCommandsToConsole(string _name, consoleCommand _command)
        {
            if (!Commands.ContainsKey(_name))
            {
                Commands.Add(_name, _command);
            }
        }



        public static Boolean checkRequired()
        {
            if (String.IsNullOrEmpty(ConsoleApp2.Properties.Settings.Default["url"].ToString()) || String.IsNullOrEmpty(ConsoleApp2.Properties.Settings.Default["output"].ToString()))
            {
                Console.WriteLine($"missing required: \n url: {String.IsNullOrEmpty(ConsoleApp2.Properties.Settings.Default["url"].ToString())}\n output: {String.IsNullOrEmpty(ConsoleApp2.Properties.Settings.Default["output"].ToString())}");
                return false;
            }
            else return true;
        }


        public static void CreateCommands()
        {
            autoreddit_cmdline.util.help help = autoreddit_cmdline.util.help.CreateCommand();
            autoreddit_cmdline.util.config config = autoreddit_cmdline.util.config.CreateCommand();
            autoreddit_cmdline.util.run run = autoreddit_cmdline.util.run.CreateCommand();
        }

        static public void validateInput(string[] args)
        {

            if (1 > args.Length)
            {
                Console.WriteLine("no arguments passed :(");
                return;
            }
            if (!Commands.ContainsKey(args[0]))
            {
                Console.WriteLine($"command {args[0]} does not exist");
                return;
            }
            else
            {
                Commands[args[0]].runCommand(args);
            }

        }
    }
}