using System.Collections.Generic;
using System;


namespace autoreddit_cmdline.util
{



    public class help : Class2.consoleCommand
    {
        public override string Name { get; protected set; }
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public help()
        {
            Name = "--help";
            Command = "--help";
            Description = "displays help";
            Help = "displays help";
            addCommandToConsole();
        }


        public override void runCommand(string[] args)
        {
            foreach (KeyValuePair<string, autoreddit_cmdline.util.Class2.consoleCommand> entry in autoreddit_cmdline.util.Class2.Commands)
            {
                Console.WriteLine($"{entry.Value.Name}: {entry.Value.Help} | {entry.Value.Description}");
            }
        }

        public static help CreateCommand()
        {
            return new help();
        }

    }
}