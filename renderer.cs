﻿using System;
using System.Speech.Synthesis;
using System.Drawing;
using System.Collections.Generic;
using Splicer.Timeline;
using Splicer.Renderer;
using System.Speech.AudioFormat;
using Splicer.WindowsMedia;

namespace autoreddit_cmdline.renderer
{
    class synth
    {
        public static SpeechSynthesizer synthesizer = new SpeechSynthesizer();
        public static List<TimeSpan> tList = new List<TimeSpan>();
    }
    
    class renderer
    {
        static SpeechAudioFormatInfo formatInfo = new SpeechAudioFormatInfo(16000, AudioBitsPerSample.Sixteen,AudioChannel.Mono);
        private static List<Bitmap> fileList = new List<Bitmap>();
        private static void renderOne(string text)
        {
            // the resolution could be anything you want although you'd have to tweak the settings yourself
            // also a bigger resolution means more pixels means more space has to be allocated in both RAM
            Bitmap canvas = new Bitmap(1280, 720);
            Graphics gfx = Graphics.FromImage(canvas);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            Font font1 = new Font(ConsoleApp2.Properties.Settings.Default["font"].ToString(),30);
            //Font font1 =;
            //main BG colour. set it as any you like
            gfx.FillRectangle(Brushes.Black, 0, 0, canvas.Width, canvas.Height);   
            Rectangle textWrap = new Rectangle(0, 0, canvas.Width, (int)Math.Ceiling(gfx.MeasureString(text, font1, canvas.Width).Height));
            textWrap.Y = (canvas.Height / 2) - (textWrap.Height / 2);
            gfx.DrawString(text, font1, Brushes.White, textWrap,stringFormat);
            fileList.Add(canvas);
        }

        private static void combineMusic()
        {
            //get ffmpeg file that describes the way to do the thing
            string ffmpeg = System.IO.File.ReadAllText("ffmpeg.txt");
            //replace {output} with the output value
            ffmpeg = ffmpeg.Replace("{output}", ConsoleApp2.Properties.Settings.Default["output"].ToString());
            //replace {music} with the music value
            ffmpeg = ffmpeg.Replace("{music}", ConsoleApp2.Properties.Settings.Default["musicFile"].ToString());
            //run command
            Console.WriteLine(ffmpeg);

        }

        public static void render()
        {
            Console.WriteLine("--START OF RENDERING PROCESS--");
            Console.WriteLine($"voice: {ConsoleApp2.Properties.Settings.Default["voice"]}\noutput: {ConsoleApp2.Properties.Settings.Default["output"]}\nfont:{ConsoleApp2.Properties.Settings.Default["font"]}\nyou: useless");
            synth.synthesizer.SelectVoice(ConsoleApp2.Properties.Settings.Default["voice"].ToString());
            synth.synthesizer.Rate = 1;
            synth.synthesizer.SetOutputToWaveFile($"./{ConsoleApp2.Properties.Settings.Default["output"]}.wav", formatInfo);
            Console.WriteLine("saving tts as wav...");
            synth.synthesizer.SpeakAsync(autoreddit_cmdline.util.helpers.builder);
            synth.synthesizer.BookmarkReached += Synthesizer_BookmarkReached;
            synth.synthesizer.SpeakCompleted += Synthesizer_SpeakCompleted;
        }

        private static void Synthesizer_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            synth.synthesizer.Dispose();
            mainRenderProcess();
        }

        private static int i = 0;
        private static void Synthesizer_BookmarkReached(object sender, BookmarkReachedEventArgs e)
        {

            // sadly this event is fired by a drunken texan and therefore might be a little of depending on the voice. 
            // I can probobly fix this but i dont feel like botghering
            renderOne(autoreddit_cmdline.util.helpers.wordList[i]);
            synth.tList.Add(e.AudioPosition);
            i++;
        }

        
        private static void mainRenderProcess()
        {
            List<IClip> clips = new List<IClip>();

            string outputFile = $"./{ConsoleApp2.Properties.Settings.Default["output"]}.mp4";
            using (ITimeline timeline = new DefaultTimeline())
            {
                IGroup group = timeline.AddVideoGroup(32, 1280, 720);
                ITrack videoTrack = group.AddTrack();
                int indx = 0;
                try
                {
                    foreach (Bitmap image in fileList)
                    {
                        double ofst;
                        if ((indx+1) > (synth.tList.Count - 1))
                        {
                            ofst = 5;
                        }
                        else
                        {
                            ofst = (synth.tList[indx+1].TotalSeconds - synth.tList[indx].TotalSeconds);
                            autoreddit_cmdline.util.helpers.log($"---meta---\nseconds\n1: {synth.tList[indx].TotalSeconds}\n2: {synth.tList[indx + 1].TotalSeconds}\n ---end meta---\nassociated phrase: {autoreddit_cmdline.util.helpers.wordList[indx]}\noffset: {ofst}\ntimestamp: {synth.tList[indx]}");
                        }
                        IClip clip = videoTrack.AddImage(image, 0, ofst);
                        clips.Add(clip);
                        indx++;
                    }
                } catch(Exception e)
                {
                    Console.Write("an error has appeared. check log file");
                    autoreddit_cmdline.util.helpers.log(e.Message);
                }

                try
                {

                    ITrack audioTrack = timeline.AddAudioGroup().AddTrack();
                    IClip audio = audioTrack.AddAudio($"{ConsoleApp2.Properties.Settings.Default["output"]}.wav", 0, videoTrack.Duration);
                    
                    //   audioTrack.AddEffect(0, audio.Duration,
                    //   StandardEffects.CreateAudioEnvelope(1.0, 0, 0, audio.Duration));

                    //read kodek since windiwsmediaprofiles.HQV is ultra gay and dumbmb
                    Console.WriteLine("render time");
                    string text = System.IO.File.ReadAllText(@"kodek.txt");
                    using (WindowsMediaRenderer renderer = new WindowsMediaRenderer(timeline, outputFile, text))
                    {
                        renderer.RenderCompleted += Renderer_RenderCompleted;
                        combineMusic();
                        renderer.Render();
                   }
                } catch(Exception e)
                {
                    Console.Write("an error has appeared. check log file");
                    autoreddit_cmdline.util.helpers.log(e.Message);
                }

                
            }

            


        }

        private static void Renderer_RenderCompleted(object sender, EventArgs e)
        {
            autoreddit_cmdline.util.helpers.log("RENDER DONE!");
        }
    }
}
