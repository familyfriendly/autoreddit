using System.Net;
using System;
using ConsoleApp2.Properties;
namespace autoreddit_cmdline.util
{
    public class run : Class2.consoleCommand
    {
        
        public override string Name { get; protected set; }
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public run()
        {
            Name = "run";
            Command = "run";
            Description = "main command. takes no arguments";
            Help = "starts rendering the video";
            addCommandToConsole();
        }

        WebClient client = new WebClient();
        public override void runCommand(string[] args)
        {         
            Console.Clear();
            if (!autoreddit_cmdline.util.Class2.checkRequired())
            {
                return;
            }
            Console.WriteLine("fetching url...");
            string data = client.DownloadString(Settings.Default["url"] + ".json");
            Console.WriteLine("deserialising JSON...");
            string content = autoreddit_cmdline.util.helpers.deserialise(data);
            Console.WriteLine("chunking data...");
            autoreddit_cmdline.util.helpers.chunk(content);

        }

        public static run CreateCommand()
        {
            return new run();
        }

    }
}