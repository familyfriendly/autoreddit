using System.IO;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ConsoleApp2.Properties;
using System.Drawing.Text;

namespace autoreddit_cmdline.util
{



    public class config : Class2.consoleCommand
    {
        public override string Name { get; protected set; }
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public config()
        {
            Name = "--config";
            Command = "--config";
            Description = "allows you to configure configurations";
            Help = "no help yet";
            addCommandToConsole();
        }


        public override void runCommand(string[] args)
        {
            if (2 > args.Length) return;
            switch (args[1])
            {
                case "url":
                    if (3 > args.Length)
                    {
                        Console.WriteLine("invalid amount of arguments passed...");
                        return;
                    }
                    Regex regex = new Regex(@"htt(ps|p):\/\/(www\.|)reddit.com\/r\/");
                    if (!regex.IsMatch(args[2]))
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("invalid url");
                        Console.ResetColor();
                        return;
                    }
                    Settings.Default["url"] = args[2];
                    break;
                case "output":
                    if (3 > args.Length)
                    {
                        Console.WriteLine("invalid amount of arguments passed...");
                        return;
                    }
                    Settings.Default["output"] = args[2];
                    break;
                case "musicFile":

                    Settings.Default["musicFile"] = args[2];
                    
                    break;
                    case "voice":
                    List<string> voices = new List<string>();
                    if (3 > args.Length)
                    {
                        
                        int i = 0;
                        foreach (object obj in autoreddit_cmdline.renderer.synth.synthesizer.GetInstalledVoices())
                        { 
                            var voice = (System.Speech.Synthesis.InstalledVoice)obj;
                            Console.WriteLine($"{i} {voice.VoiceInfo.Name}");
                            i++;
                        }

                    }
                    else
                    {
                        foreach (object obj in autoreddit_cmdline.renderer.synth.synthesizer.GetInstalledVoices())
                        {
                            var voice = (System.Speech.Synthesis.InstalledVoice)obj;
                            voices.Add(voice.VoiceInfo.Name);
                        }
                        Settings.Default["voice"] = voices[Int32.Parse(args[2])];
                    }
                    break;
                    case "font":
                    
                    using (InstalledFontCollection fontsCollection = new InstalledFontCollection())
                    {
                        System.Drawing.FontFamily[] fontFamilies = fontsCollection.Families;
                        int i = 0;
                        if (3 > args.Length)
                        {
                            foreach (System.Drawing.FontFamily font in fontFamilies)
                            {
                                Console.WriteLine($"{i}. {font.Name}");
                                i++;
                            }
                        } else
                        {
                            List<String> fontList = new List<String>();
                            foreach (System.Drawing.FontFamily font in fontFamilies)
                            {
                                fontList.Add(font.Name);
                                i++;
                            }
                            Settings.Default["font"] = fontList[Int32.Parse(args[2])];
                        }
                           
                        
                    }
                    break;
            }
            Settings.Default.Save();
        }

        public static config CreateCommand()
        {
            return new config();
        }

    }
}